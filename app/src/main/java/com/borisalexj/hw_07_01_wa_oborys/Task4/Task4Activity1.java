package com.borisalexj.hw_07_01_wa_oborys.Task4;

import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.borisalexj.hw_07_01_wa_oborys.R;

/**
 * Created by user on 4/26/2017.
 */

public class Task4Activity1 extends AppCompatActivity {
    private String TAG = "hw_07_01_wa_oborys_TAG";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_4);
    }

    public void task4StartAnimationClick(View view) {
        Log.d(TAG, "fourthStartButtonClick: ");
        Drawable drawable = ((View) view).getBackground();
            if (drawable instanceof Animatable) {
                Log.d(TAG, "fourthStartButtonClick: start");
                ((Animatable) drawable).start();
            }

    }
}
