package com.borisalexj.hw_07_01_wa_oborys.Task2_2;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.transition.ChangeBounds;
import android.transition.Slide;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.borisalexj.hw_07_01_wa_oborys.R;
import com.borisalexj.hw_07_01_wa_oborys.Task2_1.Task2_1Fragment1;

/**
 * Created by user on 4/26/2017.
 */

public class Task2_2Activity1 extends AppCompatActivity {
    FragmentManager fragmentManager;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        fragmentManager = getSupportFragmentManager();

        Task2_2Fragment1 task22Fragment1 = new Task2_2Fragment1();

        fragmentManager.beginTransaction()
                .add(R.id.task2FragmentsLayout, task22Fragment1)
                .commit();
    }

}
