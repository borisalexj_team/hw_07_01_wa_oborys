package com.borisalexj.hw_07_01_wa_oborys.Task2_2;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.transition.ChangeBounds;
import android.transition.Slide;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.borisalexj.hw_07_01_wa_oborys.R;

/**
 * Created by user on 4/26/2017.
 */

public class Task2_2Fragment1 extends Fragment {

    private ImageView imageView;
    private TextView textView;
    private Button button;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_2_1, container, false);

        view.findViewById(R.id.task2button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment task22Fragment2 = new Task2_2Fragment2();

                ChangeBounds changeBoundsTransition = new ChangeBounds();
                changeBoundsTransition.setDuration(getResources().getInteger(R.integer.anim_duration_medium));

                textView = (TextView) getActivity().findViewById(R.id.task2textView);
                imageView = (ImageView) getActivity().findViewById(R.id.task2imageView);
                button = (Button) getActivity().findViewById(R.id.task2button);

                task22Fragment2.setSharedElementEnterTransition(changeBoundsTransition);

                getFragmentManager().beginTransaction()
                        .replace(R.id.task2FragmentsLayout, task22Fragment2)
                        .addToBackStack(null)
                        .addSharedElement(textView, getString(R.string.task_2_transition_name_textView))
                        .addSharedElement(imageView, getString(R.string.task_2_transition_name_imageView))
                        .addSharedElement(button, getString(R.string.task_2_transition_name_Button))
                        .commit();
            }
        });

        return view;
    }

    public void secondFragmentOneButtonClick() {
        Log.d("TAG", "secondFragmentOneButtonClick: ");

    }
}
