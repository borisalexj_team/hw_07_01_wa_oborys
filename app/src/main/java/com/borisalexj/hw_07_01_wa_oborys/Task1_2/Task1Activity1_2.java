package com.borisalexj.hw_07_01_wa_oborys.Task1_2;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.borisalexj.hw_07_01_wa_oborys.R;

/**
 * Created by user on 4/26/2017.
 */

public class Task1Activity1_2 extends AppCompatActivity {
    private Button mButton;
    private String TAG = "hw_07_01_wa_oborys_TAG";
    private RelativeLayout rlParent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: ");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_1);
        mButton = (Button) findViewById(R.id.task1Button);

        rlParent = (RelativeLayout) findViewById(R.id.rlParentAnimOne);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void task1ButtonClick(View view) {
        Log.d(TAG, "task1ButtonClick: ");

        PropertyValuesHolder pvhX = PropertyValuesHolder.ofFloat("translationX",
                (rlParent.getWidth()-mButton.getWidth()*2)/2);
        PropertyValuesHolder pvhY = PropertyValuesHolder.ofFloat("translationY",
                -(rlParent.getHeight()-mButton.getHeight()*2)/2);
        PropertyValuesHolder pvhW = PropertyValuesHolder.ofFloat("scaleX", 1.5F);
        PropertyValuesHolder pvhH = PropertyValuesHolder.ofFloat("scaleY", 2F);
        PropertyValuesHolder pvhT = PropertyValuesHolder.ofInt("textColor", Color.RED);
        ObjectAnimator animator = ObjectAnimator.ofPropertyValuesHolder(mButton, pvhX, pvhY,
                pvhW, pvhH, pvhT);
        animator.setDuration(1000);
        animator.setInterpolator(new AccelerateInterpolator(1.5F));
        animator.start();
    }


}
