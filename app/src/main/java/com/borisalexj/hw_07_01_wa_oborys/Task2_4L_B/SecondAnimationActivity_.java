package com.borisalexj.hw_07_01_wa_oborys.Task2_4L_B;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.transition.ChangeBounds;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.borisalexj.hw_07_01_wa_oborys.R;

/**
 * Created by user on 5/3/2017.
 */

public class SecondAnimationActivity_ extends Activity {
    private FragmentManager fm;
    private FragmentTransaction ft;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_animation);

        fm = getFragmentManager();
        loadFragmentOne();
    }

    private void loadFragmentOne() {
        Fragment firstFragment = new SecondAnimationFragmentOne_();
        ft = fm.beginTransaction();
        ft.add(R.id.flContainer, firstFragment);
        ft.commit();
    }
}
