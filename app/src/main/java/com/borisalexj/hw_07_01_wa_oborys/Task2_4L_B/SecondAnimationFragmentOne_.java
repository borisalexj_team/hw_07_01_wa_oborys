package com.borisalexj.hw_07_01_wa_oborys.Task2_4L_B;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.transition.ChangeBounds;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.borisalexj.hw_07_01_wa_oborys.R;

/**
 * Created by user on 5/3/2017.
 */
public class SecondAnimationFragmentOne_ extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_2_1, container, false);

        view.findViewById(R.id.task2button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment secondFragment = new SecondAnimationFragmentTwo_();

                ChangeBounds changeBoundsTransition = new ChangeBounds();
                changeBoundsTransition.setDuration(500);

                TextView text = (TextView) getActivity().findViewById(R.id.task2textView);
                 ImageView image = (ImageView) getActivity().findViewById(R.id.task2imageView);
                 Button button = (Button) getActivity().findViewById(R.id.task2button);

                secondFragment.setSharedElementEnterTransition(changeBoundsTransition);

                getFragmentManager().beginTransaction()
                .replace(R.id.flContainer, secondFragment)
                .addToBackStack(null)
                .addSharedElement(text, getString(R.string.task_2_transition_name_textView))
                .addSharedElement(image, getString(R.string.task_2_transition_name_imageView))
                .addSharedElement(button, getString(R.string.task_2_transition_name_Button))
                .commit();
            }
        });

        return view;
    }
}