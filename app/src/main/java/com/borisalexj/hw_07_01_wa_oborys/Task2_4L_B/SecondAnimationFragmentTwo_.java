package com.borisalexj.hw_07_01_wa_oborys.Task2_4L_B;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.transition.ChangeBounds;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.borisalexj.hw_07_01_wa_oborys.R;

/**
 * Created by user on 5/3/2017.
 */
public class SecondAnimationFragmentTwo_ extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_2_2, container, false);

        final TextView text = (TextView) view.findViewById(R.id.task2textView2);
        final ImageView image = (ImageView) view.findViewById(R.id.task2imageView2);
        final Button button = (Button) view.findViewById(R.id.task2button2);

        view.findViewById(R.id.task2button2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment firstFragment = new SecondAnimationFragmentOne_();
                ChangeBounds changeBoundsTransition = new ChangeBounds();
                changeBoundsTransition.setDuration(500);

                firstFragment.setSharedElementEnterTransition(changeBoundsTransition);

                getFragmentManager().beginTransaction()
                        .replace(R.id.flContainer, firstFragment)
                        .addToBackStack(null)
                        .addSharedElement(text, getString(R.string.task_2_transition_name_textView))
                        .addSharedElement(image, getString(R.string.task_2_transition_name_imageView))
                        .addSharedElement(button, getString(R.string.task_2_transition_name_Button))
                        .commit();
            }
        });

        return view;

    }
}
