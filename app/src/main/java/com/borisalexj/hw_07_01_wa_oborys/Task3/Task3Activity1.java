package com.borisalexj.hw_07_01_wa_oborys.Task3;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.borisalexj.hw_07_01_wa_oborys.R;

/**
 * Created by user on 4/26/2017.
 */

public class Task3Activity1 extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_3_1);
//        setupWindowAnimations();

        final ImageView imageView3 = (ImageView) findViewById(R.id.task3imageView);

        ((Button) findViewById(R.id.task3button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(Task3Activity1.this, Task3Activity2.class);

                View sharedView = findViewById(R.id.task3imageView);
                String transitionName = getString(R.string.task_3_transition_name);

                ActivityOptions transitionActivityOptions = ActivityOptions.makeSceneTransitionAnimation(Task3Activity1.this, sharedView, transitionName);
                startActivity(i, transitionActivityOptions.toBundle());

            }
        });
    }

}
