package com.borisalexj.hw_07_01_wa_oborys.Task2_1;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.view.ContextThemeWrapper;
import android.transition.ChangeBounds;
import android.transition.Slide;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.borisalexj.hw_07_01_wa_oborys.R;

/**
 * Created by user on 4/26/2017.
 */

public class Task2_1Fragment1 extends Fragment {

    private View imageView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_2_1, container, false);
        ((Button) view.findViewById(R.id.task2button)).setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                secondFragmentOneButtonClick();
            }
        });

        return view;
    }


    public void secondFragmentOneButtonClick() {

        Slide slideTransition;

        slideTransition = new Slide(Gravity.RIGHT);
        slideTransition.setDuration(getResources().getInteger(R.integer.anim_duration_medium));

        Task2_1Fragment2 task21Fragment2 = new Task2_1Fragment2();
        task21Fragment2.setEnterTransition(slideTransition);
        task21Fragment2.setExitTransition(slideTransition);

        task21Fragment2.setAllowEnterTransitionOverlap(false);
        task21Fragment2.setAllowReturnTransitionOverlap(false);

        getActivity().getSupportFragmentManager().beginTransaction()
                .addToBackStack(null)
                .replace(R.id.task2FragmentsLayout, task21Fragment2)
                .commit();
    }
}
