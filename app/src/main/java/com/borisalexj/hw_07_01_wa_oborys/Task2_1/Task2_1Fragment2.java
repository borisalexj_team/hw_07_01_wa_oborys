package com.borisalexj.hw_07_01_wa_oborys.Task2_1;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.view.ContextThemeWrapper;
import android.transition.ChangeBounds;
import android.transition.Slide;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.borisalexj.hw_07_01_wa_oborys.R;

/**
 * Created by user on 4/26/2017.
 */

public class Task2_1Fragment2 extends Fragment {
    private TextView tv;
    private Button button;
    private ImageView imageView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_2_2, container, false);
        ((Button) view.findViewById(R.id.task2button2)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                secondFragmentOneButtonClick();
            }
        });


        tv = (TextView) view.findViewById(R.id.task2textView2);
        button = (Button) view.findViewById(R.id.task2button2);
        imageView = (ImageView) view.findViewById(R.id.task2imageView2);


        return view;

    }

    public void secondFragmentOneButtonClick() {

        Slide slideTransition;

        slideTransition = new Slide(Gravity.LEFT);
        slideTransition.setDuration(getResources().getInteger(R.integer.anim_duration_medium));

        Task2_1Fragment1 task21Fragment1 = new Task2_1Fragment1();

        task21Fragment1.setEnterTransition(slideTransition);
        task21Fragment1.setExitTransition(slideTransition);

        task21Fragment1.setAllowEnterTransitionOverlap(false);
        task21Fragment1.setAllowReturnTransitionOverlap(false);

        getActivity().getSupportFragmentManager().beginTransaction()
                .addToBackStack(null)
                .replace(R.id.task2FragmentsLayout, task21Fragment1)
                .commit();
    }
}
