package com.borisalexj.hw_07_01_wa_oborys.Task2_1;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.transition.ChangeBounds;
import android.transition.Slide;
import android.view.Gravity;

import com.borisalexj.hw_07_01_wa_oborys.R;

/**
 * Created by user on 4/26/2017.
 */

public class Task2_1Activity1 extends AppCompatActivity {
    FragmentManager fragmentManager;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        fragmentManager = getSupportFragmentManager();

        Slide slideTransition;

        slideTransition = new Slide(Gravity.LEFT);
        slideTransition.setDuration(getResources().getInteger(R.integer.anim_duration_medium));

        Task2_1Fragment1 task21Fragment1 = new Task2_1Fragment1();
        task21Fragment1.setEnterTransition(slideTransition);
        task21Fragment1.setExitTransition(slideTransition);

        task21Fragment1.setAllowEnterTransitionOverlap(false);
        task21Fragment1.setAllowReturnTransitionOverlap(false);

        fragmentManager.beginTransaction()
                .add(R.id.task2FragmentsLayout, task21Fragment1)
                .commit();
    }


}
