package com.borisalexj.hw_07_01_wa_oborys.Task1_1;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.widget.Button;

import com.borisalexj.hw_07_01_wa_oborys.R;

/**
 * Created by user on 4/26/2017.
 */

public class Task1Activity1_1 extends AppCompatActivity {
    private Button mButton;
    private String TAG = "hw_07_01_wa_oborys_TAG";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: ");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_1);
        mButton = (Button) findViewById(R.id.task1Button);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void task1ButtonClick(View view) {
        Log.d(TAG, "task1ButtonClick: ");

        ObjectAnimator pivotX = ObjectAnimator.ofFloat(mButton, "pivotX", 0f);
        ObjectAnimator pivotY = ObjectAnimator.ofFloat(mButton, "pivotY", 0f);
        ObjectAnimator animX = ObjectAnimator.ofFloat(mButton, View.X, 0f);
        ObjectAnimator animY = ObjectAnimator.ofFloat(mButton, View.Y, 0f);
        ObjectAnimator animHeight = ObjectAnimator.ofFloat(mButton, View.SCALE_X, 2);
        ObjectAnimator animWidth = ObjectAnimator.ofFloat(mButton, View.SCALE_Y, (float) 1.5);
        float currentTextSize = mButton.getTextSize() / getResources().getDisplayMetrics().scaledDensity;
        ObjectAnimator animTextSize = ObjectAnimator.ofFloat(mButton, "textSize", currentTextSize, currentTextSize * 2);
        ObjectAnimator animTextColor = ObjectAnimator.ofArgb(mButton, "textColor", ContextCompat.getColor(this, R.color.red));

        AnimatorSet animSetXY = new AnimatorSet();
        animSetXY.playTogether(pivotX, pivotY, animX, animY, animHeight, animWidth, animTextSize, animTextColor);

        animSetXY.setInterpolator(new AccelerateInterpolator());
        animSetXY.setDuration(1000);
        animSetXY.start();
    }


}
