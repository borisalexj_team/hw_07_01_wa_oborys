package com.borisalexj.hw_07_01_wa_oborys;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.transition.Slide;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.PopupWindow;

import com.borisalexj.hw_07_01_wa_oborys.Task1_1.Task1Activity1_1;
import com.borisalexj.hw_07_01_wa_oborys.Task1_2.Task1Activity1_2;
import com.borisalexj.hw_07_01_wa_oborys.Task2_1.Task2_1Activity1;
import com.borisalexj.hw_07_01_wa_oborys.Task2_2.Task2_2Activity1;
import com.borisalexj.hw_07_01_wa_oborys.Task2_3litvinenko.SecondAnimationActivity;
import com.borisalexj.hw_07_01_wa_oborys.Task2_4L_B.SecondAnimationActivity_;
import com.borisalexj.hw_07_01_wa_oborys.Task3.Task3Activity1;
import com.borisalexj.hw_07_01_wa_oborys.Task4.Task4Activity1;

public class StartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
    }

    public void firstStart1ButtonClick(View view) {
        Intent intent = new Intent(this, Task1Activity1_1.class);
        startActivity(intent);
    }

    public void secondOneStartButtonClick(View view) {
        Intent intent = new Intent(this, Task2_1Activity1.class);
        startActivity(intent);
    }

    public void secondTwoStartButtonClick(View view) {
        Intent intent = new Intent(this, Task2_2Activity1.class);
        startActivity(intent);
    }

    public void thirdStartButtonClick(View view) {
        Intent intent = new Intent(this, Task3Activity1.class);
        startActivity(intent);
    }

    public void fourthStartButtonClick(View view) {
        Intent intent = new Intent(this, Task4Activity1.class);
        startActivity(intent);
    }

    public void secondThreeStartButtonClick(View view) {
        startActivity(new Intent(StartActivity.this, SecondAnimationActivity.class));
    }

    public void firstStart2ButtonClick(View view) {
        Intent intent = new Intent(this, Task1Activity1_2.class);
        startActivity(intent);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void popUpStartButtonClick(View view) {
        LayoutInflater inflater = (LayoutInflater) StartActivity.this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //Inflate the view from a predefined XML layout
//        View layout = inflater.inflate(R.layout.popup1,
//                (ViewGroup) findViewById(R.id.popup_element));
        View layout = inflater.inflate(R.layout.popup1, null);
        // create a 300px width and 470px height PopupWindow
//        PopupWindow pw = new PopupWindow(layout, LinearLayout.LayoutParams.MATCH_PARENT,
//                LinearLayout.LayoutParams.MATCH_PARENT, true);
        final PopupWindow pw = new PopupWindow(layout, 300,
                300, true);

        Slide slideTransition;
        slideTransition = new Slide(Gravity.LEFT);
        slideTransition.setDuration(getResources().getInteger(R.integer.anim_duration_medium));

//        pw.setEnterTransition(slideTransition);
        (layout.findViewById(R.id.popUpButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pw.dismiss();

            }
        });

        pw.showAtLocation(view, Gravity.CENTER, 0, 0);
    }

    public void popUp2StartButtonClick(View view) {
        LayoutInflater layoutInflater
                = (LayoutInflater)getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.popup2, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        Button btnDismiss = (Button)popupView.findViewById(R.id.dismiss);
        btnDismiss.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                popupWindow.dismiss();
            }});

        popupWindow.showAsDropDown(findViewById(R.id.buttonStart3), 50, -30);

    }

    public void secondFourStartButtonClick(View view) {
        startActivity(new Intent(StartActivity.this, SecondAnimationActivity_.class));
    }

    public void FragmentDialogStartButtonClick(View view) {
        DialogFragment dialogFragment = new MyDialogFragment();
//        dialogFragment.setTargetFragment(this, 7777);
        dialogFragment.show(getSupportFragmentManager(), "DialogFragment");
    }

    public void AnimatedDialogStartButtonClick(View view) {
        Dialog dialog = new Dialog(this, R.style.CustomDialog);

// Setting the title and layout for the dialog
        dialog.setTitle(R.string.pause_menu_label);
        dialog.setContentView(R.layout.fragment_dialog);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.BOTTOM;
//        dialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        dialog.show();
//        prepareActionSheetDialog(R.layout.fragment_dialog);
//        dialog.show();
    }

    private Dialog dialog;

    private void prepareActionSheetDialog(int dialogResource) {

        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(dialogResource, null);

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this, R.style.CustomDialog);

        builder.setView(dialoglayout);
        dialog = builder.create();
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.BOTTOM;
        dialog.getWindow().getAttributes().windowAnimations = R.style.CustomDialog;

    }

    public void CancelActionSheetDialog(View view) {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

}
