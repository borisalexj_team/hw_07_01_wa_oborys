package com.borisalexj.hw_07_01_wa_oborys.Task2_3litvinenko;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.transition.ChangeBounds;
import android.widget.ImageView;
import android.widget.TextView;

import com.borisalexj.hw_07_01_wa_oborys.R;

/**
 * Created by user on 5/3/2017.
 */

public class SecondAnimationActivity extends Activity implements SecondAnimationFragmentOne.IOnMyImageClickListener {
    private FragmentManager fm;
    private FragmentTransaction ft;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_animation);

        fm = getFragmentManager();
        loadFragmentOne();
    }

    private void loadFragmentOne() {
        Fragment firstFragment = new SecondAnimationFragmentOne();
        ft = fm.beginTransaction();
        ft.add(R.id.flContainer, firstFragment);
        ft.commit();
    }

    /**
     * Change first fragment to second with animation using Transition with Shared Elements
     */
    @Override
    public void onFragmentOneImageClick() {
        Fragment secondFragment = new SecondAnimationFragmentTwo();
        ChangeBounds changeBoundsTransition = new ChangeBounds();
        changeBoundsTransition.setDuration(500);

        TextView text = (TextView) findViewById(R.id.tvText);
        ImageView image = (ImageView) findViewById(R.id.ivImage);

        secondFragment.setSharedElementEnterTransition(changeBoundsTransition);

        ft = fm.beginTransaction();
        ft.replace(R.id.flContainer, secondFragment);
        ft.addToBackStack(null);
        ft.addSharedElement(text, getString(R.string.fragm_text));
        ft.addSharedElement(image, getString(R.string.fragm_image));
        ft.commit();
    }
}
