package com.borisalexj.hw_07_01_wa_oborys.Task2_3litvinenko;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.borisalexj.hw_07_01_wa_oborys.R;

/**
 * Created by user on 5/3/2017.
 */
public class SecondAnimationFragmentOne extends Fragment implements View.OnClickListener {
    private IOnMyImageClickListener listener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (IOnMyImageClickListener) activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_second_animation_one, container, false);

        ImageView ivImage = (ImageView) view.findViewById(R.id.ivImage);

        ivImage.setOnClickListener(this);

        return  view;
    }

    @Override
    public void onClick(View view) {
        listener.onFragmentOneImageClick();
        Log.d(Constants.Log.TAG, "onClick: FragmentOne");
    }

    public interface IOnMyImageClickListener {
        void onFragmentOneImageClick();
    }
}