package com.borisalexj.hw_07_01_wa_oborys.Task2_3litvinenko;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.borisalexj.hw_07_01_wa_oborys.R;

/**
 * Created by user on 5/3/2017.
 */
public class SecondAnimationFragmentTwo extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_second_animation_two, container, false);
    }
}
